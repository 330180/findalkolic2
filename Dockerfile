FROM python:3.8
COPY . /app
WORKDIR /app
RUN python3 -m pip install flask gunicorn folium
RUN mv features.py /usr/local/lib/python3.8/site-packages/folium/

EXPOSE 5000

CMD ["/usr/local/bin/gunicorn", "--workers", "4", "--bind", "0.0.0.0:5000", "wsgi:app"]
